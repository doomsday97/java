package com.example.demo.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="customer")

public class Customer {
	
	
	
 
	    @Id
	    @Column(name="c_id")
	    @GeneratedValue(strategy=GenerationType.AUTO)
	    private int id;
	 
	    @Column(name="c_name")
	    private String name;
	 
	    @Column(name="c_company")
	    private String company;
	 
	    @Column(name="c_appointment")
	    private int appointment;
	 
	    public int getId() {
	        return id;
	    }
	 
	    public void setId(int id) {
	        this.id = id;
	    }
	 
	    public String getCustomerName() {
	        return name;
	    }
	 
	    public void setCustomerName(String customerName) {
	        this.name = customerName;
	    }
	 
	    public String getcustomerComapny() {
	        return company;
	    }
	 
	    public void setCustomerCompany(String customerCompany) {
	        this.company = customerCompany;
	    }
	 
	    public int getAppoint() {
	        return appointment;
	    }
	 
	    public void setCourse(int appointment) {
	        this.appointment = appointment;
	    }
	 
	    @Override
	    public String toString() {
	        return "Customer Details?= Id: " + this.id + ", Name: " + this.name + ", Company: " + this.company + ", Appointment: " + this.appointment;
	    }
	}

