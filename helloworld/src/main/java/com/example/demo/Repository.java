package com.example.demo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.Customer;

public interface Repository extends JpaRepository<Customer, Long>{
	

}
