package crud_controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.demo.Repository;
import com.example.demo.Model.Crud;
import com.example.demo.Model.Customer;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/api")

public class crud_controlle {
	
    @Autowired
    Repository repository;
    
    
    @GetMapping("/notes")
    public List<Customer> getAllNotes() {
        return repository.findAll();
    }
    
    @PostMapping("/notes")
    public Customer createCustomer(@Valid @RequestBody Customer cust) {
        return repository.save(cust);
    }
    
    @GetMapping("/notes/{id}")
    public Customer getNoteById(@PathVariable(value = "id") Long c_Id) {
        return repository.findById(c_Id)
                .orElseThrow(() -> new Crud("Note", "id", c_Id));
    }
    
    @PutMapping("/notes/{id}")
    public Customer updateNote(@PathVariable(value = "id") Long noteId,
                                            @Valid @RequestBody Customer noteDetails) {

        Customer note = repository.findById(noteId)
                .orElseThrow(() -> new Crud("Note", "id", noteId));

        note.setCustomerName(noteDetails.getCustomerName());
        note.setContent(noteDetails.getContent());

        Customer updatedNote = repository.save(note);
        return updatedNote;
    }
    
    


}
